# Used technologies  
Retrofit  
Room  
LiveData  
RxJava  
ViewModel  
Dagger  
  
## Used language  
Kotlin  

## Used Api  
https://openweathermap.org/

### ApiKey
Located at internal/Config.kt

### .Apk file
Located at app/build/outputs/apk/debug/  

---
author: Melichar Jan   
---