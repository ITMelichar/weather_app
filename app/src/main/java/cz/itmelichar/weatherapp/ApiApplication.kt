package cz.itmelichar.weatherapp

import android.app.Application
import cz.itmelichar.weatherapp.data.database.ApiDatabase

class ApiApplication : Application() {

    companion object {
        lateinit var instance: ApiApplication
        lateinit var database: ApiDatabase
    }

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        database = ApiDatabase.invoke(this)
    }

}