package cz.itmelichar.weatherapp.data.database

import androidx.room.*
import io.reactivex.Single

//database requests
@Dao
interface DataDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: List<DataEntity>)

    @Query("SELECT * from data")
    fun queryData(): Single<List<DataEntity>>

    @Query("DELETE FROM data")
    fun deleteAllData()

}
