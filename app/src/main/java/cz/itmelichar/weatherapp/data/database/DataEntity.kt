package cz.itmelichar.weatherapp.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

//database structure
@Entity(tableName = "data")
data class DataEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,
        @ColumnInfo(name = "date")
    val date: String,
        @ColumnInfo(name = "sunrise")
    val sunrise: String,
        @ColumnInfo(name = "sunset")
    val sunset: String,
        @ColumnInfo(name = "day_temp")
    val dayTemp: String,
        @ColumnInfo(name = "night_temp")
    val nightTemp: String,
        @ColumnInfo(name = "weather_desc")
    val description: String
    )
