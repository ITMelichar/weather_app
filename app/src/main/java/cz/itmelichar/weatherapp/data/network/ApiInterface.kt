package cz.itmelichar.weatherapp.data.network

import cz.itmelichar.weatherapp.model.ApiResult
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
//api call
    @GET("onecall")
    fun getWeather(
        @Query("appid") apiKey: String,
        @Query("lat") latitude: String,
        @Query("lon") longitude: String,
        @Query("lang") lang: String,
        @Query("exclude") exclude: String,
        @Query("units") units: String
    ): Flowable<ApiResult>
}