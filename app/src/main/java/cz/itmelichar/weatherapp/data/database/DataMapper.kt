package cz.itmelichar.weatherapp.data.database

import cz.itmelichar.weatherapp.internal.toDate
import cz.itmelichar.weatherapp.internal.toTime
import cz.itmelichar.weatherapp.model.Daily
import cz.itmelichar.weatherapp.model.Temp
import cz.itmelichar.weatherapp.model.Weather

//from database
fun DataEntity.toData() = Daily(
    toDate(this.date),
    toTime(this.sunrise),
    toTime(this.sunset),
    Temp(this.dayTemp + "°C", this.nightTemp + "°C"),
    listOf(Weather(this.description))
)

fun List<DataEntity>.toDataList() = this.map { it.toData() }

//into database
fun Daily.toDataEntity() = DataEntity(
        date = this.dt,
        sunrise = this.sunrise,
        sunset = this.sunset,
        dayTemp = this.temp.day,
        nightTemp = this.temp.night,
        description = this.weather[0].description
)

fun List<Daily>.toDataEntityList() = this.map { it.toDataEntity() }