package cz.itmelichar.weatherapp.viewmodel

import androidx.lifecycle.ViewModel
import cz.itmelichar.weatherapp.di.DaggerAppComponent
import cz.itmelichar.weatherapp.repository.ApiRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ApiViewModel: ViewModel() {

    @Inject
    lateinit var repository: ApiRepository

    private val compositeDisposable by lazy { CompositeDisposable() }

    init {
        DaggerAppComponent.create().inject(this)
        compositeDisposable.add(repository.fetchDataFromDatabase())
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}