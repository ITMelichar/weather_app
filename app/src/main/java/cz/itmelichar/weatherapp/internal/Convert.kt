package cz.itmelichar.weatherapp.internal

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

//Convert string to date format
    @SuppressLint("SimpleDateFormat")
    fun toDate(s: String): String {
        return try {
            val sdf = SimpleDateFormat("dd.MM.yyyy")
            val netDate = Date(s.toLong() * 1000)
            sdf.format(netDate)
        } catch (e: Exception) {
            e.toString()
        }
    }

//Convert string to time format
    @SuppressLint("SimpleDateFormat")
    fun toTime(s: String): String {
        return try {
            val sdf = SimpleDateFormat("HH:mm:ss")
            val netDate = Date(s.toLong() * 1000)
            sdf.format(netDate)
        } catch (e: Exception) {
            e.toString()
        }
    }

//get Timestamp
fun actualTimestamp(): Int {
    return (System.currentTimeMillis() / 1000).toInt()
}

