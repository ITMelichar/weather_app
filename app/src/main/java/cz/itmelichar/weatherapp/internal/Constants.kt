package cz.itmelichar.weatherapp.internal

//api url
const val BASE_URL = "https://api.openweathermap.org/data/2.5/"

//name of locale database
const val DATABASE_NAME = "weather.db"

//sending to API request
const val LAT = "50.2096835"
const val LON = "17.19133814"
const val EXCLUDE = "minutely,alerts,hourly"
const val UNITS = "metric"

//localization - not "const val" (changing with app start)
var LANG = "en"