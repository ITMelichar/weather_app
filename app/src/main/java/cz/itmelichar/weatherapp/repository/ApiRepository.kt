package cz.itmelichar.weatherapp.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.itmelichar.weatherapp.ApiApplication
import cz.itmelichar.weatherapp.data.database.toDataEntityList
import cz.itmelichar.weatherapp.data.database.toDataList
import cz.itmelichar.weatherapp.data.network.ApiInterface
import cz.itmelichar.weatherapp.di.DaggerAppComponent
import cz.itmelichar.weatherapp.internal.*
import cz.itmelichar.weatherapp.model.ApiResult
import cz.itmelichar.weatherapp.model.Daily
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject


class ApiRepository {

    @Inject
    lateinit var apiService: ApiInterface

    private val _data by lazy { MutableLiveData<List<Daily>>() }
    val daily: LiveData<List<Daily>>
        get() = _data

    private val _isInProgress by lazy { MutableLiveData<Boolean>() }
    val isInProgress: LiveData<Boolean>
        get() = _isInProgress

    private val _isError by lazy { MutableLiveData<Boolean>() }
    val isError: LiveData<Boolean>
        get() = _isError


    init {
        DaggerAppComponent.create().inject(this)
    }

    //insert data from database
    private fun insertData(): Disposable {
        return apiService.getWeather(API_KEY, LAT, LON, LANG, EXCLUDE, UNITS)
            .subscribeOn(Schedulers.io())
            .subscribeWith(subscribeToDatabase())
    }

    private fun subscribeToDatabase(): DisposableSubscriber<ApiResult> {
        return object : DisposableSubscriber<ApiResult>() {

            override fun onNext(apiResult: ApiResult?) {
                if (apiResult != null) {
                    val entityList = apiResult.daily.toList().toDataEntityList()
                    ApiApplication.database.apply {
                        dataDao().insertData(entityList)
                    }
                }
            }
            override fun onError(t: Throwable?) {
                _isInProgress.postValue(true)
                Log.e("insertData()", "Result error: ${t?.message}")
                _isError.postValue(true)
                _isInProgress.postValue(false)
            }

            override fun onComplete() {
                Log.v("insertData()", "insert success")
                getApiQuery()
            }

        }

    }

    //select data from database
    private fun getApiQuery(): Disposable {
        return ApiApplication.database.dataDao()
            .queryData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                    { dataEntityList ->
                        _isInProgress.postValue(true)
                        if (dataEntityList != null && dataEntityList.isNotEmpty()) {
                            if((dataEntityList[0].date.toInt() + 50400) < actualTimestamp()) {
                                //not tested because data from API are fetched now (date of app start)
                                deleteDataQuery()
                                insertData()
                                Log.v("deleteData()", "Deleted")
                            } else {
                                _isError.postValue(false)
                                _data.postValue(dataEntityList.toDataList())
                                Log.v("postValue()", "Posted")
                            }
                        } else {
                            insertData()
                            Log.v("insertData()", "Inserted")
                        }
                        _isInProgress.postValue(false)
                    },
                    {
                        _isInProgress.postValue(true)
                        Log.e("getApiQuery()", "Database error: ${it.message}")
                        _isError.postValue(true)
                        _isInProgress.postValue(false)
                    }

            )
    }
    //delete all data from database
    private fun deleteDataQuery() {
        return ApiApplication.database.dataDao()
                .deleteAllData()
    }

    fun fetchDataFromDatabase(): Disposable = getApiQuery()

}