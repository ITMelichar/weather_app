package cz.itmelichar.weatherapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import cz.itmelichar.weatherapp.R
import cz.itmelichar.weatherapp.databinding.ItemBinding
import cz.itmelichar.weatherapp.model.Daily

class ApiAdapter(
    private val daily: ArrayList<Daily>
    ) : RecyclerView.Adapter<ApiViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ApiViewHolder {
            val itemBinding: ItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item,
                parent,
                false
            )
            return ApiViewHolder(itemBinding)
        }

        override fun onBindViewHolder(holder: ApiViewHolder, position: Int) {
            holder.itemBinding.daily = daily[position]
        }

        override fun getItemCount(): Int = daily.size

        fun setUpData(data: List<Daily>) {
            this.daily.clear()
            this.daily.addAll(data)
            notifyDataSetChanged()
        }
    }

class ApiViewHolder(
    val itemBinding: ItemBinding
    ): RecyclerView.ViewHolder(itemBinding.root)