package cz.itmelichar.weatherapp.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import cz.itmelichar.weatherapp.R
import cz.itmelichar.weatherapp.di.DaggerAppComponent
import cz.itmelichar.weatherapp.internal.LANG
import cz.itmelichar.weatherapp.view.adapter.ApiAdapter
import cz.itmelichar.weatherapp.viewmodel.ApiViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var apiAdapter: ApiAdapter

    private val viewModel: ApiViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initVars()

        DaggerAppComponent.create().inject(this)

        setUpRecyclerView()

        observeLiveData()

    }

    private fun setUpRecyclerView() {
        recycler_view.apply {
            setHasFixedSize(true)
            itemAnimator = DefaultItemAnimator()
            adapter = apiAdapter
        }
    }

    private fun observeLiveData() {
        observeInProgress()
        observeIsError()
        observeApiList()
    }

    private fun observeInProgress() {
        viewModel.repository.isInProgress.observe(this, { isLoading ->
            isLoading.let {
                if(it) {
                    empty_text.visibility = View.GONE
                    recycler_view.visibility = View.GONE
                    fetch_progress.visibility = View.VISIBLE
                } else {
                    fetch_progress.visibility = View.GONE
                }
            }
        })
    }

    private fun observeIsError() {
        viewModel.repository.isError.observe(this, { isError ->
            isError.let {
                if (it) {
                    disableViewsOnError()
                } else {
                    empty_text.visibility = View.GONE
                    fetch_progress.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun disableViewsOnError() {
        fetch_progress.visibility = View.VISIBLE
        empty_text.visibility = View.VISIBLE
        recycler_view.visibility = View.GONE
        apiAdapter.setUpData(emptyList())
        fetch_progress.visibility = View.GONE
    }

    private fun observeApiList() {
        viewModel.repository.daily.observe(this, { daily ->
            daily.let {
                if (it != null && it.isNotEmpty()) {
                    fetch_progress.visibility = View.VISIBLE
                    recycler_view.visibility = View.VISIBLE
                    apiAdapter.setUpData(it)
                    empty_text.visibility = View.GONE
                    fetch_progress.visibility = View.GONE
                } else {
                    disableViewsOnError()
                }
            }
        })
    }

    private fun initVars() {
        LANG = getString(R.string.lang)
    }

}