package cz.itmelichar.weatherapp.di

import cz.itmelichar.weatherapp.repository.ApiRepository
import cz.itmelichar.weatherapp.view.ui.MainActivity
import cz.itmelichar.weatherapp.viewmodel.ApiViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(apiRepository: ApiRepository)

    fun inject(viewModel: ApiViewModel)

    fun inject(mainActivity: MainActivity)

}