package cz.itmelichar.weatherapp.di

import cz.itmelichar.weatherapp.data.network.ApiInterface
import cz.itmelichar.weatherapp.data.network.ApiService
import cz.itmelichar.weatherapp.model.Daily
import cz.itmelichar.weatherapp.repository.ApiRepository
import cz.itmelichar.weatherapp.view.adapter.ApiAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideApi(): ApiInterface = ApiService.getClient()

    @Provides
    fun provideApiRepository() = ApiRepository()

    @Provides
    fun provideListData() = ArrayList<Daily>()

    @Provides
    fun provideApiAdapter(data: ArrayList<Daily>): ApiAdapter = ApiAdapter(data)

}