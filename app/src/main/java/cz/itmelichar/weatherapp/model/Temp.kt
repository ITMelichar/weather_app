package cz.itmelichar.weatherapp.model

data class Temp(
        val day: String,
        val night: String
)