package cz.itmelichar.weatherapp.model

data class Daily(
    val dt: String,
    val sunrise: String,
    val sunset: String,
    val temp: Temp,
    val weather: List<Weather>
)
