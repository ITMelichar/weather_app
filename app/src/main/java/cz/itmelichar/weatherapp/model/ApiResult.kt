package cz.itmelichar.weatherapp.model

data class ApiResult(
    val `daily`: List<Daily>
)