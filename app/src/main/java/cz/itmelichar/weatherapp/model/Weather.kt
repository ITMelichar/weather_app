package cz.itmelichar.weatherapp.model

data class Weather (
        val description: String,
)